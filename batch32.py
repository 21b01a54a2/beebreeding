hexa_coad= [[1, 0, -1], [0, 1, -1], [-1, 1, 0], [-1, 0, 1], [0, -1, 1], [1, -1, 0]]
RADIUS = 58
EDGES=6
while(True):
    hexa_num1, hexa_num2 = map(int, input().split())
    if hexa_num1 == 0 and hexa_num2 == 0:
        break
    else:
        print(min_distance(createGrid(RADIUS), hexa_num1, hexa_num2))